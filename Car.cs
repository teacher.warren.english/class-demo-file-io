﻿namespace ClassDemoFileIO
{
    public class Car
    {
        public string Name { get; set; }
        public string Brand { get; set; }
        public double Price { get; set; }

        public Car()
        {

        }
        public Car(string name)
        {
            Name = name;
        }
    }
}