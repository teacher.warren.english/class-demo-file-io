﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ClassDemoFileIO
{
    internal class Program
    {
        const string TEXT_CARS_FILENAME = @"cars.txt";
        const string CSV_CARS_FILENAME = @"cars.csv";
        static void Main(string[] args)
        {
            // WriteToTextFile();
            // ReadFromTextFile();
            WriteToCsvFile();
            ReadFromCsvFile();
        }

        static List<Car> GetCars()
        {
            List<Car> cars = new List<Car>() {
                new Car() { Brand = "Ford", Name = "Mustang", Price = 150.99 },
                new Car() { Brand = "Dodge", Name = "Viper", Price = 280.99 },
                new Car() { Brand = "VW", Name = "Golf", Price = 9600.99 },
                new Car() { Brand = "Nissan", Name = "GTR", Price = 9600.99 },
            };

            return cars;
        }

        static List<Car> GetCarNamesOnly()
        {
            List<Car> cars = new List<Car>() {
                new Car("Mustang"),
                new Car("Viper"),
                new Car("Golf"),
                new Car("GTR"),
            };

            return cars;
        }

        // write to a text file
        static void WriteToTextFile()
        {
            List<Car> cars = GetCars();

            try
            {
                using StreamWriter sr = new StreamWriter(@"cars.txt");
                foreach (var car in cars)
                {
                    sr.WriteLine(car.Brand + " " + car.Name);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        // read from a text file
        static void ReadFromTextFile()
        {
            try
            {
                using StreamReader sr = new StreamReader(@"cars.txt");

                string line = string.Empty;

                while((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                }
                
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // write to a csv
        static void WriteToCsvFile()
        {
            try
            {
                using StreamWriter sw = new StreamWriter(CSV_CARS_FILENAME);
                using CsvWriter writer = new CsvWriter(sw, CultureInfo.InvariantCulture);

                writer.WriteRecords(GetCars());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        // read from csv
        static void ReadFromCsvFile()
        {
            try // Alt + ⬆ / Alt + ⬇
            {
                using StreamReader sr = new StreamReader(CSV_CARS_FILENAME);
                using CsvReader reader = new CsvReader(sr, CultureInfo.InvariantCulture);

                IEnumerable<Car> carsFromCsvFile = reader.GetRecords<Car>();

                foreach (var car in carsFromCsvFile)
                {
                    Console.WriteLine($"{car.Brand} {car.Name} {car.Price}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
